package com.exercicio.cartoes.repositories;

import com.exercicio.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento,Integer> {
}
