package com.exercicio.cartoes.repositories;

import com.exercicio.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
