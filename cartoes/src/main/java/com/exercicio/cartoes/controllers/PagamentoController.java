package com.exercicio.cartoes.controllers;

import com.exercicio.cartoes.models.Pagamento;
import com.exercicio.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public ResponseEntity<Pagamento> salvarPagamento(@RequestBody @Valid Pagamento pagamento){
        try {
            Pagamento pagamentoRetorno = pagamentoService.salvarPagamento(pagamento);
            return ResponseEntity.status(201).body(pagamentoRetorno);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public ResponseEntity<ArrayList> pesquisarPagamentos(@PathVariable int id_cartao) {

        try {
            ArrayList<Pagamento> retorno = pagamentoService.buscarPagamentos(id_cartao);
            return ResponseEntity.status(200).body(retorno);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }

    }

}
