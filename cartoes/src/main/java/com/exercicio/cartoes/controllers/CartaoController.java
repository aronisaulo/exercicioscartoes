package com.exercicio.cartoes.controllers;

import com.exercicio.cartoes.models.Cartao;
import com.exercicio.cartoes.models.dto.*;
import com.exercicio.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")

public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    // Maneira passada pelo Instrutor
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse salvarCartao(@RequestBody @Valid CreateCartaoRequest createCartaoRequest){

        Cartao cartao = mapper.toCartao(createCartaoRequest);

        cartao = cartaoService.salvarCartao(cartao);

        return mapper.toCreateCartaoResponse(cartao);
    }


    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update (@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest){
        updateCartaoRequest.setNumero(numero);
        Cartao retorno = mapper.toCartao(updateCartaoRequest);
        retorno = cartaoService.atualizaCartao(retorno);
        return mapper.toUpdateCartaoResponse(retorno);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse pequisarNumeroCartao(@PathVariable String numero){
        Cartao retorno = cartaoService.pesquisarCartao(numero);
        return mapper.toGetCartaoResponse(retorno);
    }


}
