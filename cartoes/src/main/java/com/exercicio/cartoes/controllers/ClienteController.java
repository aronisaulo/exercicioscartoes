package com.exercicio.cartoes.controllers;

import com.exercicio.cartoes.models.Cliente;
import com.exercicio.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> cadastrarCliente(@RequestBody  @Valid Cliente cliente) {
        try {
            Cliente retorno = clienteService.salvarCliente(cliente);
            return ResponseEntity.status(201).body(retorno);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
     public ResponseEntity<Cliente> consultarCliente(@PathVariable Integer id) {
        try {
            Optional<Cliente> retorno = clienteService.pesquisarCliente(id);
            return ResponseEntity.status(200).body(retorno.get());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
