package com.exercicio.cartoes.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class CreateCartaoRequest {
    @JsonProperty("numero")
    private String numero;
    @NotNull
    @JsonProperty("clienteId")
    private int clienteID;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteID() {
        return clienteID;
    }

    public void setClienteID(int clienteID) {
        this.clienteID = clienteID;
    }
}
