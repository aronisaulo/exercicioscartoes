package com.exercicio.cartoes.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCartaoResponse {

    private int id;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("clienteId")
    private int clienteId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
