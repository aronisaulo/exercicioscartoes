package com.exercicio.cartoes.models.dto;

import com.exercicio.cartoes.models.Cartao;
import com.exercicio.cartoes.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao (CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero((createCartaoRequest.getNumero()));

        Cliente cliente = new Cliente() ;
        cliente.setId(createCartaoRequest.getClienteID());
        cartao.setCliente(cliente);

        return cartao;
    }


    public CreateCartaoResponse toCreateCartaoResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteID(cartao.getCliente().getId());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }


    public Cartao toCartao(UpdateCartaoRequest updateCartaoRequest) {
        Cartao creditCard = new Cartao();

        creditCard.setNumero(updateCartaoRequest.getNumero());
        creditCard.setAtivo(updateCartaoRequest.getAtivo());

        return creditCard;
    }

    public UpdateCartaoResponse toUpdateCartaoResponse(Cartao creditCard) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(creditCard.getId());
        updateCartaoResponse.setNumero(creditCard.getNumero());
        updateCartaoResponse.setClienteId(creditCard.getCliente().getId());
        updateCartaoResponse.setAtivo(creditCard.getAtivo());

        return updateCartaoResponse;
    }

    public GetCartaoResponse toGetCartaoResponse(Cartao creditCard) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(creditCard.getId());
        getCartaoResponse.setNumero(creditCard.getNumero());
        getCartaoResponse.setClienteId(creditCard.getCliente().getId());

        return getCartaoResponse;
    }
}
    



