package com.exercicio.cartoes.services;

import com.exercicio.cartoes.exception.ClienteNotFoundException;
import com.exercicio.cartoes.models.Cliente;
import com.exercicio.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

   public Optional<Cliente> pesquisarCliente(Integer id) {
        return clienteRepository.findById(id);
    }

    public Cliente getById(Integer id){
        Optional<Cliente> byId = clienteRepository.findById(id);
        if (!byId.isPresent()) {
            throw  new ClienteNotFoundException();
        }
        return  byId.get();
    }
}
