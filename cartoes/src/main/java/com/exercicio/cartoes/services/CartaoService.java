package com.exercicio.cartoes.services;

import com.exercicio.cartoes.exception.CartaoNotFoundException;
import com.exercicio.cartoes.models.Cartao;
import com.exercicio.cartoes.models.Cliente;
import com.exercicio.cartoes.repositories.CartaoRepository;
import com.exercicio.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

     @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao)  {
        Cliente cliente = clienteService.getById(cartao.getCliente().getId());
        cartao.setCliente(cliente);
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao atualizaCartao(Cartao cartao) {

        Cartao databaseCartao = getByNumero(cartao.getNumero());
        databaseCartao.setAtivo(cartao.getAtivo());
        return cartaoRepository.save(databaseCartao);
    }

    public Cartao pesquisarCartao(String numero)  {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
         if (!cartaoOptional.isPresent()){
             throw new CartaoNotFoundException();
         }
         return cartaoOptional.get();
    }

    public Cartao getByNumero(String number) {
        /* Exemplo em 1 linha
        CreditCard creditCard = creditCardRepository.findByNumbero(numbero)
                .orElseThrow(CreditCardNotFoundException::new);
        */

        // nosso código normal
        Optional<Cartao> byId = cartaoRepository.findByNumero(number);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }


}
