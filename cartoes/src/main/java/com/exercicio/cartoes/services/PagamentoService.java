package com.exercicio.cartoes.services;

import com.exercicio.cartoes.models.Pagamento;
import com.exercicio.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento salvarPagamento(Pagamento pagamento){
        return pagamentoRepository.save(pagamento);
    }

    public ArrayList<Pagamento> buscarPagamentos(int cartaoID) throws Exception{
        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findAll();

        for (Pagamento pagamentoRetorno : pagamentoIterable){
            if (pagamentoRetorno.getCartao_id()==cartaoID)
                return montaPagamentos(pagamentoIterable,cartaoID);
            else
                throw  new Exception("Não encontrado o id do Cartão");
        }
        return  null;

    }


    private ArrayList<Pagamento>  montaPagamentos(Iterable<Pagamento> pagamentoIterable , int cartaoID){
        ArrayList<Pagamento> listaPagamentos = new ArrayList<Pagamento>();
        for (Pagamento pagamentoRetorno : pagamentoIterable) {
            if (pagamentoRetorno.getCartao_id() == cartaoID)
                listaPagamentos.add(pagamentoRetorno);
        }
        return listaPagamentos;

    }

}
